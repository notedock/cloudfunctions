/* eslint-disable max-len */
// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

import admin = require("firebase-admin");
import functions = require("firebase-functions");

admin.initializeApp();

exports.updateNotesCountWhenCreated = functions.firestore
  .document("users/{userUUID}/folders/{folderUUID}/notes/{noteUUID}")
  .onCreate((snapshot: any, context: any) => {
    const userUUID = context.params.userUUID;
    const folderUUID = context.params.folderUUID;
    const docRef = admin.firestore().collection("users").doc(userUUID).collection("folders").doc(folderUUID);

    return docRef.get().then((querySnapshot:any) => {
      const ref = admin.firestore().collection("users").doc(userUUID).collection("folders").doc(folderUUID).collection("notes");

      ref.get().then((querySnaphot:any) => {
        const notesCount = querySnaphot.size;
        const updateData = {notesCount: notesCount};
        return docRef.update(updateData);
      });
    });
  });

exports.updateNotesCountWhenDeleted = functions.firestore
  .document("users/{userUUID}/folders/{folderUUID}/notes/{noteUUID}")
  .onDelete((snapshot: any, context: any) => {
    const userUUID = context.params.userUUID;
    const folderUUID = context.params.folderUUID;
    const docRef = admin.firestore().collection("users").doc(userUUID).collection("folders").doc(folderUUID);

    return docRef.get().then((querySnapshot:any) => {
      const ref = admin.firestore().collection("users").doc(userUUID).collection("folders").doc(folderUUID).collection("notes");

      ref.get().then((querySnaphot:any) => {
        const notesCount = querySnaphot.size;
        const updateData = {notesCount: notesCount};
        return docRef.update(updateData);
      });
    });
  });


exports.deleteEmptyNote = functions.firestore
  .document("users/{userUUID}/folders/{folderUUID}/notes/{noteUUID}")
  .onUpdate((change: any, context: any) => {
    const note = change.after.data();

    const title: string = note.title.trim();
    const description: string = note.description.trim();

    if (title.length === 0 && description.length === 0) {
      return change.after.ref.delete();
    }
    return null;
  });
