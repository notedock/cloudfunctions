Simple cloud functions that delete empty notes and recalculate number of notes in folder when a note is created/deleted. Functions use nodejs version 18 and are gen1.

Basic instructions on deploying can be found in official [docs](https://firebase.google.com/docs/functions/get-started?gen=1st).
